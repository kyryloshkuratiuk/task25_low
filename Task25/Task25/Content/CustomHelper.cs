﻿using Microsoft.Ajax.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Task25.Models;

namespace Task25.Content
{
    public static class CustomHelper
    {
        public static MvcHtmlString ConvertToList(this HtmlHelper helper, Form form)
        {
            var div = new TagBuilder("div");
            string result = String.Empty;
            result += "<ul class=\"result-text\">";
            result += "<li>Name: " + form.Name + "</li>";
            result += "<li>Surname: " + form.Surname + "</li>";
            result += "<li>Email: " + form.Email + "</li>";
            result += "<li>Gender: " + form.Gender + "</li>";
            result += "<li>Agreement: " + form.Agreement + "</li>";
            div.InnerHtml = result;
            var html = div.ToString(TagRenderMode.Normal);

            return MvcHtmlString.Create(html);
        }

        public static MvcHtmlString DisplayArticles(this HtmlHelper helper, IEnumerable<Article> list)
        {
            string result = String.Empty;
            foreach (var article in list)
            {
                string preview = "";
                if (article.Content.Length > 200)
                {
                    preview = article.Content.Substring(0, 200);
                }
                else
                {
                    preview = article.Content;
                }

                result += "<article>";
                result += "<div><h1>" + article.Header + "</h1></div>";
                result += "<div><p>" + preview + $"@Html.Action(\"Update me!\", \"Details\", {article})><br/> Continue reading...</a> </p></div>";
                result += "<p>" + article.Date + "</p>";
                result += "</article>";
            }
            return new MvcHtmlString(result);
        }
    }
}