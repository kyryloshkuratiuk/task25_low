﻿using System;

namespace Task25.Models
{
    public class Review
    {
        public int Id { get; set; }
        public string AuthorName { get; set; }
        public string Content { get; set; }
        public DateTime Date { get; set; }
    }
}