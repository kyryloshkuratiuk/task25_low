﻿namespace Task25.Models
{
    public class Form
    {
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Email { get; set; }
        public string Gender { get; set; }
        public string Agreement { get; set; }
    }
}