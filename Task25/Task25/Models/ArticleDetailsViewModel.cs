﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Task25.Models
{
    public class ArticleDetailsViewModel
    {
        public Article Article { get; set; }
        public string PageTitle { get; set; }
    }
}