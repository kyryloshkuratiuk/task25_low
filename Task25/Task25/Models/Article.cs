﻿using System;

namespace Task25.Models
{
    public class Article
    {
        public int Reference { get; set; }
        public string Header { get; set; }
        public string Content { get; set; }
        public DateTime Date { get; set; }
    }
}