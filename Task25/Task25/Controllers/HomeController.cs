﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Collections.Generic;
using System.Web.Mvc;
using Task25.Models;
using Task25.BLL;

namespace Task25.Controllers
{
    public class HomeController : Controller
    {
        readonly HomeService homeService;

        /// <summary>
        /// HomeController constructor
        /// </summary>
        public HomeController()
        {
            homeService = new HomeService();
        }

        /// <summary>
        /// Action to redirect to main page
        /// </summary>
        public ActionResult Index()
        {
            var articles = homeService.GetListOrderesdByPublicationDate();

            var articlesMapped = new List<Models.Article>();

            foreach (var article in articles)
            {
                articlesMapped.Add(
                    new Article
                    {
                        Reference = article.Id,
                        Header = article.Header,
                        Content = article.Content,
                        Date = article.Date
                    }
                    );
            }

            return View(articlesMapped);
        }

        /// <summary>
        /// Action to redirect to the main page
        /// </summary>
        public ActionResult Create()
        {
            return View();
        }

        /// <summary>
        /// Action to add new article to the database
        /// </summary>
        /// <param name="article">Article object to add</param>
        [HttpPost]
        public ActionResult Create(Article article)
        {
            if (ModelState.IsValid)
            {
                homeService.Create(article.Header, article.Content);
                return RedirectToAction("Index");
            }

            return View(article);
        }

        [HttpGet]
        public ViewResult Details(Article article)
        {
            ArticleDetailsViewModel articleDetailsViewModel = new ArticleDetailsViewModel()
            {
                Article = article,
                PageTitle = article.Header
            };
            return View(articleDetailsViewModel);
        }
    }
}