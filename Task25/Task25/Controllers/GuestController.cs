﻿using System.Collections.Generic;
using System.Web.Mvc;
using Task25.Models;
using Task25.BLL;
using System.Text.RegularExpressions;

namespace Task25.Controllers
{
    public class GuestController : Controller
    {
        readonly GuestService guestService;

        /// <summary>
        /// GuestController contstructor
        /// </summary>
        public GuestController()
        {
            guestService = new GuestService();
        }

        /// <summary>
        /// Action to redirect to the main page
        /// </summary>
        public ActionResult Index()
        {
            var reviews = guestService.GetListOrderedByPublicationDate();

            var reviewsMapped = new List<Models.Review>();

            foreach (var review in reviews)
            {
                reviewsMapped.Add(
                    new Review
                    {
                        AuthorName = review.AuthorName,
                        Content = review.Content,
                        Date = review.Date
                    }
                    );
            }

            return View(reviewsMapped);
        }

        /// <summary>
        /// Action to add a new review to the database
        /// </summary>
        [HttpPost]
        public ActionResult Create()
        {
            bool flag = true;
            Regex textRegex = new Regex("[A-Za-z]+");

            try
            {
                Match match = textRegex.Match(Request.Form["name"]);

                if (!match.Success)
                {
                    flag = false;
                }

                match = textRegex.Match(Request.Form["textReview"]);

                if (!match.Success)
                {
                    flag = false;
                }
            }
            catch
            {
                flag = false;
            }

            if (flag == true && ModelState.IsValid)
            {
                guestService.Create(Request.Form["name"], Request.Form["textReview"]);
            }

            return RedirectToAction("Index");

        }
    }
}