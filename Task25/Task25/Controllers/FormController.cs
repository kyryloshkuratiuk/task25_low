﻿using System.Collections.Generic;
using System.Web.Mvc;
using Task25.Models;
using Task25.BLL;
using System.Text.RegularExpressions;
using System;

namespace Task25.Controllers
{
    public class FormController : Controller
    {
        readonly FormService formService;

        static List<string> validationErrors = new List<string>();

        /// <summary>
        /// FormController constructor
        /// </summary>
        public FormController()
        {
            formService = new FormService();
        }

        /// <summary>
        /// Action to redirect to main page
        /// </summary>
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Action to redirect to result page
        /// </summary>
        [HttpGet]
        public ActionResult Result()
        {
            var forms = formService.GetListOrderedByPublicationDate();

            var formsMapped = new List<Models.Form>();

            foreach (var form in forms)
            {
                formsMapped.Add(
                    new Form
                    {
                        Name = form.Name,
                        Surname = form.Surname,
                        Email = form.Email,
                        Gender = form.Gender,
                        Agreement = form.Agreement
                    }
                    );
            }

            return View(formsMapped);
        }

        /// <summary>
        /// Action to add a new form to the database
        /// </summary>
        [HttpPost]
        public ActionResult Create()
        {
            validationErrors = new List<string>();

            bool flag = true;
            Regex nameRegex = new Regex("[A-Za-z]+");

            try
            {
                Match match = nameRegex.Match(Request.Form["FirstName"]);
                if (!match.Success)
                {
                    validationErrors.Add("Provided name is not valid.");
                    flag = false;
                }

                match = nameRegex.Match(Request.Form["LastName"]);
                if (!match.Success)
                {
                    validationErrors.Add("Provided surname is not valid");
                    flag = false;
                }

                Regex emailRegex = new Regex(@"(\w.?)+@[A-Za-z]+.[a-z]+");
                match = emailRegex.Match(Request.Form["Email"]);
                if (!match.Success)
                {
                    validationErrors.Add("Provided email is not valid");
                    flag = false;
                }

                if (Request.Form["Gender"] == null)
                {
                    validationErrors.Add("Provided gender is not valid");
                    flag = false;
                }
            }
            catch (Exception e)
            {
                flag = false;
                validationErrors.Add(e.Message);
            }

            string agreement = String.Empty;
            if (Request.Form["Agreement"] == "false")
                agreement = "Rejected";
            else agreement = "Accepted";

            if (flag == true && ModelState.IsValid)
            {
                formService.Create(Request.Form["FirstName"], Request.Form["LastName"], Request.Form["Email"], Request.Form["Gender"], agreement);
                return RedirectToAction("Result");
            }
            else return RedirectToAction("Error");
        }

        /// <summary>
        /// Action to redirect to Error page
        /// </summary>
        public ActionResult Error()
        {
            return View(validationErrors);
        }
    }
}