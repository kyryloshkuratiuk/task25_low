﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using Task25.DAL.Context;
using Task25.DAL.Entities;
using Task25.DAL.Interfaces;

namespace Task25.DAL.Repositories
{
    public class SQLFormRepository : IRepository<Form>
    {
        private readonly DataContext db;

        /// <summary>
        /// SQLFormRepository constructor
        /// </summary>
        public SQLFormRepository()
        {
            this.db = new DataContext();
        }

        /// <summary>
        /// Method to get list of Forms
        /// </summary>
        /// <returns>IEnumberable of Form class</returns>
        public IEnumerable<Form> GetList()
        {
            return db.Forms;
        }

        /// <summary>
        /// Method to get an object of Form by ID
        /// </summary>
        /// <param name="id">ID of Form to get</param>
        /// <returns></returns>
        public Form GetObject(int id)
        {
            return db.Forms.Find(id);
        }

        /// <summary>
        /// Method to create a Form object
        /// </summary>
        /// <param name="item">Form object to create</param>
        public void Create(Form item)
        {
            db.Forms.Add(item);
        }

        /// <summary>
        /// Method to update Form
        /// </summary>
        /// <param name="item">Form to update</param>
        public void Update(Form item)
        {
            db.Entry(item).State = EntityState.Modified;
        }

        /// <summary>
        /// Method to delete Form by ID
        /// </summary>
        /// <param name="id"></param>
        public void Delete(int id)
        {
            Form Form = db.Forms.Find(id);
            if (Form != null)
                db.Forms.Remove(Form);
        }

        /// <summary>
        /// Method to Save changes in the database
        /// </summary>
        public void Save()
        {
            db.SaveChanges();
        }

        public bool disposed = false;

        /// <summary>
        /// Disposal method
        /// </summary>
        /// <param name="disposing">Disposal boolean</param>
        public virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    db.Dispose();
                }
            }
            this.disposed = true;
        }

        /// <summary>
        /// Disposal method
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
