﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using Task25.DAL.Context;
using Task25.DAL.Entities;
using Task25.DAL.Interfaces;

namespace Task25.DAL.Repositories
{
    /// <summary>
    /// Article repository
    /// </summary>
    public class SQLArticleRepository : IRepository<Article>
    {
        /// <summary>
        /// DataContext declaration
        /// </summary>
        private readonly DataContext db;

        /// <summary>
        /// SQLArticleRepository constructor
        /// </summary>
        public SQLArticleRepository()
        {
            this.db = new DataContext();
        }

        /// <summary>
        /// Method to get list of Articles
        /// </summary>
        /// <returns>IEnumerable of Article class</returns>
        public IEnumerable<Article> GetList()
        {
            return db.Articles;
        }

        /// <summary>
        /// Method to get object of Article
        /// </summary>
        /// <param name="id">ID of article</param>
        /// <returns>Object of an article</returns>
        public Article GetObject(int id)
        {
            return db.Articles.Find(id);
        }

        /// <summary>
        /// Method to add an article to the database
        /// </summary>
        /// <param name="item">An article to add</param>
        public void Create(Article item)
        {
            db.Articles.Add(item);
        }

        /// <summary>
        /// Method to update an article
        /// </summary>
        /// <param name="item"></param>
        public void Update(Article item)
        {
            db.Entry(item).State = EntityState.Modified;
        }

        /// <summary>
        /// Method to delete an article by id
        /// </summary>
        /// <param name="id">ID of an article to delete</param>
        public void Delete(int id)
        {
            Article article = db.Articles.Find(id);
            if (article != null)
                db.Articles.Remove(article);
        }

        /// <summary>
        /// Method to save changes in the database
        /// </summary>
        public void Save()
        {
            db.SaveChanges();
        }

        public bool disposed = false;

        /// <summary>
        /// Disposal method
        /// </summary>
        /// <param name="disposing">Disposal boolean</param>
        public virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    db.Dispose();
                }
            }
            this.disposed = true;
        }

        /// <summary>
        /// Disposal method
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
