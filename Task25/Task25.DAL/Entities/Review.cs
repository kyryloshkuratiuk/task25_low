﻿using System;

namespace Task25.DAL.Entities
{
    /// <summary>
    /// Entity of Review class
    /// </summary>
    public class Review
    {
        public int Id { get; set; }
        public string AuthorName { get; set; }
        public string Content { get; set; }
        public DateTime Date { get; set; }
    }
}
