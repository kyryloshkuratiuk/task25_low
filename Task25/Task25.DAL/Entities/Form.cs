﻿namespace Task25.DAL.Entities
{
    /// <summary>
    /// Entity of Form class
    /// </summary>
    public class Form
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Email { get; set; }
        public string Gender { get; set; }
        public string Agreement { get; set; }
    }
}
