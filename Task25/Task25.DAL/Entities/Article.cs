﻿using System;

namespace Task25.DAL.Entities
{
    /// <summary>
    /// Entity of article class
    /// </summary>
    public class Article
    {
        public int Id { get; set; }
        public string Header { get; set; }
        public string Content { get; set; }
        public DateTime Date { get; set; }
    }
}
