﻿using System.Data.Entity;
using Task25.DAL.Entities;

namespace Task25.DAL.Context
{
    public class DataContext : DbContext
    {
        /// <summary>
        /// DataContext constructor
        /// </summary>
        public DataContext() : base("DataContext") { }

        /// <summary>
        /// DbSet of Article objects
        /// </summary>
        public DbSet<Article> Articles { get; set; }

        /// <summary>
        /// DbSet of Review objects
        /// </summary>
        public DbSet<Review> Reviews { get; set; }

        /// <summary>
        /// DbSet of Form obcjets
        /// </summary>
        public DbSet<Form> Forms { get; set; }
    }
}
