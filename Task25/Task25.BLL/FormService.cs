﻿using System.Collections.Generic;
using Task25.DAL.Entities;
using Task25.DAL.Interfaces;
using Task25.DAL.Repositories;

namespace Task25.BLL
{
    public class FormService
    {
        readonly IRepository<Form> db;

        /// <summary>
        /// FormController constructor
        /// </summary>
        public FormService()
        {
            db = new SQLFormRepository();
        }

        public IEnumerable<Form> GetListOrderedByPublicationDate()
        {
            return db.GetList();
        }

        /// <summary>
        /// Action to redirect to result page
        /// </summary>
        public IEnumerable<Form> Result()
        {
            return db.GetList();
        }

        /// <summary>
        /// Action to add a new form to the database
        /// </summary>
        public void Create(string name, string surname, string email, string gender, string agreement)
        {
            var newForm = new DAL.Entities.Form()
            {
                Name = name,
                Surname = surname,
                Email = email,
                Gender = gender,
                Agreement = agreement
            };

            db.Create(newForm);
            db.Save();
        }
    }
}
