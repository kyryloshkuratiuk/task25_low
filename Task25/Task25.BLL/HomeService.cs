﻿using System;
using System.Collections.Generic;
using System.Linq;
using Task25.DAL.Entities;
using Task25.DAL.Interfaces;
using Task25.DAL.Repositories;

namespace Task25.BLL
{
    public class HomeService
    {
        readonly IRepository<Article> db;

        /// <summary>
        /// HomeController constructor
        /// </summary>
        public HomeService()
        {
            db = new SQLArticleRepository();
        }

        /// <summary>
        /// Action to redirect to the main page
        /// </summary>
        public IEnumerable<Article> GetListOrderesdByPublicationDate()
        {
            return db.GetList().OrderByDescending(x => x.Date);
        }

        /// <summary>
        /// Action to add new article to the database
        /// </summary>
        /// <param name="article">Article object to add</param>
        public void Create(string header, string content)
        {
            var newArticle = new DAL.Entities.Article()
            {
                Header = header,
                Content = content,
                Date = DateTime.Now
            };

            db.Create(newArticle);
            db.Save();
        }
    }
}
