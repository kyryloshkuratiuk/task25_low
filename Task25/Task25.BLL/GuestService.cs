﻿using System;
using System.Collections.Generic;
using Task25.DAL.Entities;
using Task25.DAL.Interfaces;
using Task25.DAL.Repositories;

namespace Task25.BLL
{
    public class GuestService
    {
        readonly IRepository<Review> db;

        /// <summary>
        /// GuestController contstructor
        /// </summary>
        public GuestService()
        {
            db = new SQLReviewRepository();
        }

        public IEnumerable<Review> GetListOrderedByPublicationDate()
        {
            return db.GetList();
        }

        /// <summary>
        /// Action to add a new review to the database
        /// </summary>
        public void Create(string authorName, string content)
        {
            var newReview = new DAL.Entities.Review()
            {
                AuthorName = authorName,
                Content = content,
                Date = DateTime.Now
            };

            db.Create(newReview);
            db.Save();
        }
    }
}
